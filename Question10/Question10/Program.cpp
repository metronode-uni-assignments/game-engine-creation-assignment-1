#include <iostream>

using namespace std;

void main() {
	string outputString = "Do you know who loves C++? XX does!";
	cout << outputString << endl;
	cout << "What is your name?: ";
	string name;
	cin >> name;
	outputString.replace(27, 2, name);
	cout << outputString << endl;
	string oldName = name;
	cout << "Give me a longer name!: ";
	cin >> name;
	outputString.replace(27, oldName.length(), name);
	cout << outputString << endl;
}