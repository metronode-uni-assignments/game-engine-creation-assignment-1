#include <iostream>
using namespace std;

void main()
{
	const int hariboPackets = 40;
	const int studentsPresent = 14;
	int packetsToShare = hariboPackets / studentsPresent;
	int packetsLeftOver = hariboPackets % studentsPresent;

	cout << "You can share " << packetsToShare << " packets between " << studentsPresent << " students, and you will have " << packetsLeftOver << " packets left over." << endl;
}