#include <iostream>
#include <Windows.h>

using namespace std;

int main() {
	bool repeating = true;
	while (repeating)
	{
		int number = NULL;
		while (number == NULL || number <= 0 || number > 10)
		{
			cout << "Please enter a whole number between 1 and 10, inclusive: ";
			cin >> number;
		}

		for (int i = 0; i < number + 1; i -= -1)
		{
			cout << i << "\t";
		}
		cout << endl;
		for (int i = 0; i < number; i -= -1)
		{
			cout << i + 1 << "\t";
			for (int j = 0; j < i + 1; j -= -1)
			{
				cout << "*\t";
			}
			cout << endl;
		}

		cout << "Would you like to try again? [Y/N]: ";
		string reply;
		cin >> reply;
		if (reply == "Y" || reply == "y")
			continue;
		else
		{
			repeating = false;
			cout << "Goodbye!";
			Sleep(1000);
		}


	}
	return 0;
}