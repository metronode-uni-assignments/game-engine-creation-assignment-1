#include <iostream>

using namespace std;

void main()
{
	typedef int date;
	enum Month {
		JANUARY = 1,
		FEBUARY,
		MARCH,
		APRIL,
		MAY,
		JUNE,
		JULY,
		AUGUST,
		SEPTEMBER,
		OCTOBER,
		NOVEMBER,
		DECEMBER
	};
	date the_date = 15;
	Month the_month = APRIL;

	cout << "The date is " << the_date << " and the month is " << the_month << endl;
}
