#include <iostream>

using namespace std;

void main() {
	const float c_pi = 3.14;
	float radius = 22.0f;
	float area_of_circle = c_pi * pow(radius, 2);

	cout << "The area of a circle with radius " << radius << " is " << area_of_circle << endl;
}