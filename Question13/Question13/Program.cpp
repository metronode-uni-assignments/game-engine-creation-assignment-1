#include <iostream>

using namespace std;

int main()
{
	int num = 0;
	int& rNum = num;

	cout << "Input a number: ";
	cin >> rNum;
	cout << "Current value: " << rNum << endl;
	rNum += 25;
	cout << "New value: " << rNum << endl;
	cout << "Input a new number: ";
	cin >> rNum;
	cout << "Current value: " << rNum << endl;
	rNum -= 25;
	cout << "New value: " << rNum << endl;

	return 0;
}