#include <iostream>
using namespace std;

void main() {
	float num1, num2;
	cout << "Please enter a number: ";
	cin >> num1;
	cout << "Please enter another number: ";
	cin >> num2;

	float sum = num1 * num2;
	float mean = (((float)num1 + (float)num2) / 2);
	cout << "The sum of " << num1 << " and " << num2 << " is " << sum \
		<< " and the mean of both numbers is " << mean << "." << endl;
}