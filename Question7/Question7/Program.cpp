#include <iostream>

using namespace std;


int numOfEvenNumbers = 0;
int numOfOddNumbers = 0;

int sumOfEvenNumbers = 0;
int sumOfOddNumbers = 0;

void OddOrEven(int number)
{
	if (number % 2 == 0)
	{
		numOfEvenNumbers++;
		sumOfEvenNumbers += number;
	}
	else
	{
		numOfOddNumbers++;
		sumOfOddNumbers += number;
	}
}

int main()
{
	int numbers[10];
	cout << "Please enter 10 whole numbers below: " << endl;

	for (int i = 0; i < 10; i++)
	{
		cin >> numbers[i];
		OddOrEven(numbers[i]);
	}

	cout << "Total odd numbers: " << numOfOddNumbers << endl;
	cout << "Total even numbers: " << numOfEvenNumbers << endl;
	cout << "Sum of odd numbers: " << sumOfOddNumbers << endl;
	cout << "Sum of even numbers: " << sumOfEvenNumbers << endl;

	return 0;

}