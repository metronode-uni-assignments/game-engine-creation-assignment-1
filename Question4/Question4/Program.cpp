#include <iostream>

using namespace std;

void main() {
	int selection;
	cout << "Please choose an option: 1 for Sunny | 2 for Cloudy | 3 for Raining | 4 to Exit >> ";
	cin >> selection;

	switch (selection)
	{
	case 1:
		cout << "Don't forget to wear sunscreen!" << endl;
		break;
	case 2:
		cout << "Cloudy, how exciting. Not!" << endl;
		break;
	case 3:
		cout << "Don't forget a coat!" << endl;
		break;
	case 4:
		exit(0);
	}
}